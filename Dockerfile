# aicacia/build-agent-python:${PYTHON_VERSION}

FROM aicacia/docker-kube-helm:19.03-1.17-3.1.2

RUN apt update \
  && apt install -y \
  build-essential \
  zlib1g-dev \
  libssl-dev

ENV PYENV_ROOT /usr/local/pyenv
ENV PATH $PYENV_ROOT/bin:$PATH

RUN git clone https://github.com/pyenv/pyenv.git $PYENV_ROOT

ARG PYTHON_VERSION=3.8.2
RUN pyenv install $PYTHON_VERSION
RUN pyenv global $PYTHON_VERSION

RUN echo 'export PYENV_ROOT="$PYENV_ROOT"' >> $HOME/.bashrc
RUN echo 'export PATH="$PYENV_ROOT/bin:$PATH"' >> $HOME/.bashrc

RUN echo 'eval "$(pyenv init -)"' >> $HOME/.bashrc