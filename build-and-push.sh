#!/bin/bash

repo="aicacia/build-agent-python"
set -e

function build_and_push() {
  local version=$1
  local full_version=$2
  
  docker build --build-arg PYTHON_VERSION=${full_version} -t ${repo}:${full_version} .
  docker push ${repo}:${full_version}

  docker tag ${repo}:${full_version} ${repo}:${version}
  docker push ${repo}:${version}

  docker tag ${repo}:${version} ${repo}:latest
  docker push ${repo}:latest
}

build_and_push "2" "2.7.17"
build_and_push "3" "3.8.2"